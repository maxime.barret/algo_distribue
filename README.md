# Projet d'algorithme distribué

Le sujet du projet est de traiter un volume important de fichiers LAS et d'en extraire des informations selon un processus optimisé.

Ce projet a été réalisé en trinome par Maxime Barret, Papa-Mama Sow et Loïc Lepannetier. Pour des raisons de matériel, ce projet a du être réalisé en utilisant qu'un seul ordinateur.


## Q1 : Compute and sort the number of occurrences of all the curves with the same name
    
M. Wlodarczyk nous aide en nous fournissant une fonction qui permet de lire les fichiers LAS et d'en extraire uniquement les noms des colonnes et les mettre en lignes grâce à un FlatMap.

```python
def variable_count(filename):
            try : 
                cli = get_storage_service()   
                bucket = cli.bucket(bucketname)       
                blob = bucket.blob(filename)           
                datastr = blob.download_as_string(client = cli)               
                lf = lasio.read(datastr.decode('latin-1'),ignore_header_errors=True,ignore_data=True)
                return lf.df().columns
            except: 
                return ["Error"]
```

Malheuresement cette fonction ne marche que sur un 'sc.parallelize' de 500 fichiers LAS. Si l'on essaye de l'appliquer sur tous les fichiers cela ne marche pas pour des raisons de mémoire il semblerait. Et si on essaye de faire une boucle de 500 fichiers ou plus, cela ne marche pas car les méthodes pour fusionner les résultats de chaques boucles telle que 'union' par exemple ne marchent pas non plus et dans ce dernier cas les logs Spark rendent très complexe la résolution de ce non fonctionnement.

Après ces échécs nous nous sommes orienté vers une solution plus fastidieuse mais simple en fonctionnement. Selon nous, le faits que les essais précédents ne fonctionnent pas provient de la lecture du fichier LAS (ceci est une hypothèse, nous n'avons pas pu la prouver), du coup nous avons changé le fait de devoir passer par la lecture de fichier LAS. Nous démarrons par une conversion de tous les fichiers LAS en fichiers Parquets (étape que nous pouvons désactivé en cas d'exécution multiple), puis nous itérons à travers les fichiers pour en extraire les colonnes. Une fois les colonnes extraites, nous transformont notre tableau dans un format (clé, valeur) avec pour clé le nom de la colonne et une valeur = 1. Puis nous sommons sur les clés afin d'optenir le nombre d'occurence de chaque clé.

conversion LAS - Parquets :
```python
if generate_parquets :
            t_gp = time.time()  # Temps de generation
            for f in lasfiles :

                if not (f.replace('.las', '') in os.listdir('/p/SLM_files/parquets')) : # Pour 'OOMKilled with exit code 137'
                    
                    print(f + ' a traiter ...')
                    blob = bucket.blob(f)
                    datastr = blob.download_as_string(client = cli)
                    # print("lecture...")
                    try :
                        lasio_file = lasio.read(datastr.decode('latin-1'), ignore_header_errors=True) #lasio.LAS
                        # print("....lecture ok")
                        df_data = lasio_file.df() # Pandas DF
                        # print(df_data.head())
                        df_data.reset_index(drop=False, inplace=True)

                        df_data.to_parquet('/p/SLM_files/parquets/{}'.format(f.replace('.las', '')))
                        print(" ... fichier "+f+" traité")

                    except :
                        print("fichier "+ f + " non traité")
            print("Temps pour generer les fichiers parquets : ", time.time() - t_gp, "s avec", nb_executor,"worker")
```


Fonction adaptée à Parquets
```python
def variable_count_custom(filename):
                df = pa.read_parquet('/p/SLM_files/parquets/'+filename)
                return df.columns
```


## Q2 : Estimate distribution of the curve with the same name

Afin d'estimer la distribution de chaque type de courbe, l'idée a été de générer une table par type de courbe et d'y aggréger toutes les courbes de tous les fichiers et ainsi appliquer un model de régression de SparkML. La génération des tables a pris plus de temps que prévu et l'appplication d'un model de SparkML nécessite du temps et malheuresement nous en manquons. Nous avons donc opté pour une moyenne de chaque ligne de chaques tables et d'en sortir une Dataframe sauvegardé en CSV qui peut par la suite être importé, analysé et visualisé sur un notebook comme nous l'avons fait pour la question 5.

Script de génération des tables des courbes :
```python
 files = os.listdir('/p/SLM_files/parquets')

            for f in files :
                print("traitement du fichiers ", f)
                df = pa.read_parquet('/p/SLM_files/parquets/' + f)

                cols = list(df.columns)

                # Récupération du nom de la colonne qui sert d'indexe (profondeur du forage)
                if 'DEPT' in cols :
                    index = 'DEPT'
                elif 'DEPTH' in cols :
                    index = 'DEPTH'
                elif 'DEPTH:1' in cols :
                    index = 'DEPTH:1'
                elif 'DEPTH:2' in cols :
                    index = 'DEPTH:2'
                else :
                    print("pas d'indices trouvé, skip. colonnes présentes : ", cols)
                    continue

                # on enlève la colonne d'indexe car on veut pas créer sa table
                cols.remove(index) # cas de multiple index non traité


                for col in cols :
                    filename = col + '.csv'

                    if filename in os.listdir('/p/SLM_files/curves') :
                        # ajout d'une colonne dans une table éxistante
                        print("append de la table ", col)
                        df_curve = pa.read_csv('/p/SLM_files/curves/' +filename, sep=';')
                        dept = list(df_curve.columns)[0]
                        df_curve = df_curve.merge(df[[index, col]], how='outer', left_on=dept , right_on=index)

                    else :
                        # création de la table
                        print('création de la table de la courbe ', col)
                        df_curve = df[[index, col]]

                    df_curve.to_csv('/p/SLM_files/curves/'+filename, sep=';') # obligé de sauvegardé en csv car parquet nécéssite un dossier par nom sinon
```

Algorithme de moyennation des tables :
```python
df_read = pa.read_csv('/p/SLM_files/curves/'+f)

                cols = list(df_read.columns)
                
                # on prend les table qui ont aux moins 5 courbes car sinon cela n'est pas intéressent et prend du temps de calcul pour rien
                if len(cols) > 6 :

                    if first_time :
                        index = cols[0]
                        df_read[curve_name] = df_read[cols[1:]].mean(axis=1)
                        df_analyse = df_read[[index, curve_name]]
                        first_time = False
                    else :
                        df_analyse[curve_name] = df_read[cols[1:]].mean(axis=1)
                else :
                    print("table {} non prise en compte".format(curve_name))
```


## Q4 : Provide total time with various number of worker for all the previous steps

Afin de mesurer le temps d'exécution de chaque étape, nous avons simplement utilisé la librairie time de python et généré des timer en début et fin d'étape. Ces temps sont affichés aux côtés du nombre de worker utilisé pour réaliser le calcul. Ces indicateurs nous fournissent des retours sur le déroulement des calculs et peuvent servir d'indicateurs pour optimiser la distribution du calcul.

Extrait de point de mesure :
```python
 t1_q2 = time.time()     # Temps d'analyse des donnees
 """
    Script
 """
print("Temps pour analyser les donnees : ", time.time() - t1_q2, "s avec", nb_executor, "worker")
```


## Q5 : Propose an innovative visualisation of the results ( python )

Afin de visualiser les résultats, nous avons choisi d'utiliser la librairie *Bokeh*. Bokeh offre un design plus esthétique et une variété d'outils non négligeable par apport à une librairie plus classique telle que *matplotlib*. 
Pour représenter les occurences de chaques courbes nous avons choisi d'opter pour un histogramme trié afin de bien montrer la prédominance de quelques courbes par rapport aux autres. L'histogramme est stocké dans un fichier HTML (Visualisation/c_sort.html) et possède l'outil 'HoverTool' qui indique les valeurs d'abcisse et d'ordonné du curseur de la souris.

## Points d'améliorations

- **Traiter tous les fichiers LAS** (chercher des nouvelles méthodes d'ingestion des données)
- **Réaliser un modèle SparkML de régression des données**
- **Utiliser plus de méthodes de Spark** (parallélisation des données, partitionnement, et variation du nombre de worker)